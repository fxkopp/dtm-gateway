<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessInstancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_instances', function (Blueprint $table) {

            $table->string('process_instance_id');

            $table->string('business_key')->unique();
            
            $table->string('process_definition_key');

            $table->string('process_definition_id');

            $table->string('name')->nullable();

            $table->text('description')->nullable();

            $table->enum('state', ['ACTIVE', 'SUSPENDED', 'COMPLETED', 'EXTERNALLY_TERMINATED', 'INTERNALLY_TERMINATED'])->nullable();

            $table->timestamps();

            // Indexes
            $table->primary('process_instance_id');

            $table->index('business_key');

            $table->index('process_definition_key');

            $table->index('process_definition_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_instances');
    }
}
