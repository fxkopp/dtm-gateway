<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityInstancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_instances', function (Blueprint $table) {
            // Id
            $table->id();

            // Activity
            $table->string('activity_instance_id')->nullable();

            $table->string('activity_id');

            $table->string('activity_name');

            $table->string('activity_type');

            $table->boolean('activity_canceled')->default(false);

            // Activity source
            $table->enum('activity_source', ['api', 'user']);

            // Activity data
            $table->json('activity_data')->nullable();

            // References
            $table->string('process_instance_id');

            $table->string('called_process_instance_id')->nullable();

            // Timestamps

            $table->dateTimeTz('start_time');

            $table->dateTimeTz('end_time')->nullable();

            // Indexes
            $table->unique('activity_instance_id');

            $table->foreign('process_instance_id')->references('process_instance_id')->on('process_instances');

            $table->index('activity_id');

            $table->index('activity_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_instances');
    }
}
