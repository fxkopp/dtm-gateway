<?php 

namespace App\Services\Camunda;

use App\Models\ActivityInstance;
use App\Models\ProcessInstance;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use OpenAPI\Client\Api\ProcessInstanceApi;
use OpenAPI\Client\ApiException;
use OpenAPI\Client\Model\ProcessInstanceModificationDto;
use OpenAPI\Client\Model\ProcessInstanceModificationInstructionDto;
use OpenAPI\Client\Model\SuspensionStateDto;

class ProcessInstanceService
{
    private ProcessInstanceApi $api;

    public function __construct(ProcessInstanceApi $api)
    {
        $this->api = $api;
        $this->instructions = collect([]);
    }

    /**
     * Activate or suspend process instance
     * 
     * @param string $id ProcessInstanceId
     * @param bool $suspended 
     * @return ProcessInstanceService 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function updateSuspensionState(string $id, bool $suspended): self
    {
        $this->api->updateSuspensionStateById(
            $id, 
            new SuspensionStateDto([
                'suspended' => $suspended,
            ])
        );

        return $this;
    }

    /**
     * Return Collection of ActivityInstances from ActivityInstanceTree
     * 
     * @param string $id ProcessInstanceId
     * @return Collection Collection of AcitivityInstances
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getActivityInstances(string $id): \Illuminate\Support\Collection
    {
        $activityInstances = collect([]);

        $activityInstanceTree = $this->api->getActivityInstanceTree($id);

        foreach ($activityInstanceTree->getChildActivityInstances() as $childActivityInstance) {
            $activityInstances->push(ActivityInstance::createFromActivityInstanceDto($childActivityInstance));
        }

        return $activityInstances;
    }

    /**
     * Submits the list of modification instructions to change a process instance's execution state
     * 
     * @param ProcessInstance $processInstance
     * @param null|string $annotation An arbitrary text annotation set for auditing reasons
     * @return ProcessInstanceService 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function modifyProcessInstance(ProcessInstance $processInstance, ?string $annotation = ''): self
    {
        $this->api->modifyProcessInstance(
            $processInstance->process_instance_id, 
            new ProcessInstanceModificationDto([
                'skip_custom_listeners' => true,
                'skip_io_mappings' => true,
                'instructions' => 
                    $processInstance->modificationInstructions->map(function ($item) {
                        return new ProcessInstanceModificationInstructionDto($item);
                    })->toArray(),
                'annotation' => $annotation,
            ])
        );

        return $this;
    }
}