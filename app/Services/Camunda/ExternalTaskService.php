<?php 

namespace App\Services\Camunda;

use App\Models\ExternalTask;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use OpenAPI\Client\Api\ExternalTaskApi;
use OpenAPI\Client\ApiException;

class ExternalTaskService
{
    private ExternalTaskApi  $api;

    public function __construct(ExternalTaskApi $api)
    {
        $this->api = $api;
    }

    /**
     * Get List 
     * 
     * @see https://github.com/fxkopp/camunda-php-sdk/blob/main/docs/Api/ExternalTaskApi.md#getExternalTasks
     * 
     * @param array $args 
     * @return Collection 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getExternalTasks($args = []): \Illuminate\Support\Collection
    {
        return collect($this->api->getExternalTasks(
            data_get($args, 'external_task_id'),
            data_get($args, 'external_task_id_in'),
            data_get($args, 'topic_name'),
            data_get($args, 'worker_id'),
            data_get($args, 'locked'),
            data_get($args, 'not_locked'),
            data_get($args, 'with_retries_left'),
            data_get($args, 'no_retries_left'),
            data_get($args, 'lock_expiration_after'),
            data_get($args, 'lock_expiration_before'),
            data_get($args, 'activity_id_in'),
            data_get($args, 'execution_id'),
            data_get($args, 'process_instance_id'),
            data_get($args, 'process_instance_id_in'),
            data_get($args, 'process_definition_id'),
            data_get($args, 'tenant_id_in'),
            data_get($args, 'active'),
            data_get($args, 'suspended'),
            data_get($args, 'priority_higher_than_or_equals'),
            data_get($args, 'priority_lower_than_or_equals'),
            data_get($args, 'sort_by'),
            data_get($args, 'sort_order'),
            data_get($args, 'first_result'),
            data_get($args, 'max_results'),
        ))->map(function ($item) {
            return ExternalTask::createFromExternalTaskDto($item);
        });
    }

    /**
     * Get List of Failed External Tasks
     * 
     * @param array $args 
     * @return Collection 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getFailedExternalTasks($args = []): \Illuminate\Support\Collection
    {
        // Override args to only retrieve failed tasks
        return $this->getExternalTasks(
            array_merge($args, [
                'no_retries_left' => (bool) true,
            ])
        );
    }

    /**
     * Sets the number of retries left to execute an external task by id. If retries are set to 0, an incident is created.
     * 
     * @see https://github.com/fxkopp/camunda-php-sdk/blob/main/docs/Api/ExternalTaskApi.md#setExternalTaskResourceRetries
     * 
     * @param string $id External Task Id
     * @param int $retries Number of Retries
     * @return void 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function setExternalTaskResourceRetries(string $id, int $retries): void
    {
        $this->api->setExternalTaskResourceRetries($id, new \OpenAPI\Client\Model\RetriesDto([
            'retries' => $retries,
        ]));
    }
}