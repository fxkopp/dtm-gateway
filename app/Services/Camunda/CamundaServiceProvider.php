<?php

namespace App\Services\Camunda;

use Illuminate\Support\ServiceProvider;

use OpenAPI\Client\Configuration;

use App\Services\Camunda\ProcessDefinitionService;
use App\Services\Camunda\HistoricActivityInstanceService;
use App\Services\Camunda\IncidentService;
use App\Services\Camunda\ExternalTaskService;
use App\Services\Camunda\HistoricProcessInstanceService;
use App\Services\Camunda\ProcessInstanceService;

use GuzzleHttp\Client;
use OpenAPI\Client\Api\HistoricActivityInstanceApi;
use OpenAPI\Client\Api\HistoricProcessInstanceApi;
use OpenAPI\Client\Api\IncidentApi;
use OpenAPI\Client\Api\ExternalTaskApi;
use OpenAPI\Client\Api\ProcessDefinitionApi;
use OpenAPI\Client\Api\ProcessInstanceApi;

class CamundaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        /**
         * Configuration
         */
        $config = tap(new Configuration())
            ->setHost(config('services.camunda.host'));

        /**
         * APIs
         */
        app()->singleton(ProcessDefinitionService::class, function () use ($config) {
            return new ProcessDefinitionService(new ProcessDefinitionApi(
                new Client(),
                $config
            ));
        });

        app()->singleton(HistoricActivityInstanceService::class, function() use ($config) {
            return new HistoricActivityInstanceService(new HistoricActivityInstanceApi(
                new Client(),
                $config
            ));
        });

        app()->singleton(HistoricProcessInstanceService::class, function() use ($config) {
            return new HistoricProcessInstanceService(new HistoricProcessInstanceApi(
                new Client(),
                $config
            ));
        });

        app()->singleton(ProcessInstanceService::class, function() use ($config) {
            return new ProcessInstanceService(new ProcessInstanceApi(
                new Client(),
                $config
            ));
        });

        app()->singleton(IncidentService::class, function() use ($config) {
            return new IncidentService(new IncidentApi(
                new Client(),
                $config
            ));
        });

        app()->singleton(ExternalTaskService::class, function() use ($config) {
            return new ExternalTaskService(new ExternalTaskApi(
                new Client(),
                $config
            ));
        });
    }
}
