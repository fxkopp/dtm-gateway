<?php 

namespace App\Services\Camunda;

use App\Models\ProcessInstance;
use OpenAPI\Client\Api\HistoricProcessInstanceApi;

class HistoricProcessInstanceService
{
    private HistoricProcessInstanceApi $api;

    public function __construct(HistoricProcessInstanceApi $api)
    {
        $this->api = $api;
    }

    public function getHistoricProcessInstance(string $id): ProcessInstance
    {
        return ProcessInstance::createFromHistoricProcessInstanceDto($this->api->getHistoricProcessInstance($id));
    }
}