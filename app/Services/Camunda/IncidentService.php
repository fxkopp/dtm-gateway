<?php 

namespace App\Services\Camunda;

use App\Models\Incident;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use OpenAPI\Client\Api\IncidentApi;
use OpenAPI\Client\ApiException;

class IncidentService
{
    private IncidentApi $api;

    public function __construct(IncidentApi $api)
    {
        $this->api = $api;
    }

    /**
     * Get List 
     * 
     * @see https://github.com/fxkopp/camunda-php-sdk/blob/main/docs/Api/IncidentApi.md#getincidents
     * 
     * @param array $args 
     * @return Collection 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getIncidents($args = []): \Illuminate\Support\Collection
    {
        return collect($this->api->getIncidents(
            data_get($args, 'incident_id'),
            data_get($args, 'incident_type'),
            data_get($args, 'incident_message'),
            data_get($args, 'incident_message_like'),
            data_get($args, 'process_definition_id'),
            data_get($args, 'process_definition_key_in'),
            data_get($args, 'process_instance_id'),
            data_get($args, 'execution_id'),
            data_get($args, 'incident_timestamp_before'),
            data_get($args, 'incident_timestamp_after'),
            data_get($args, 'activity_id'),
            data_get($args, 'failed_activity_id'),
            data_get($args, 'cause_incident_id'),
            data_get($args, 'root_cause_incident_id'),
            data_get($args, 'configuration'),
            data_get($args, 'tenant_id_in'),
            data_get($args, 'job_definition_id_in'),
            data_get($args, 'sort_by'),
            data_get($args, 'sort_order'),
        ))->map(function ($item) {
            return Incident::createFromIncidentDto($item);
        });
    }
}