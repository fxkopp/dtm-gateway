<?php

namespace App\Services\Camunda;

use App\Models\ActivityInstance;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use OpenAPI\Client\Api\HistoricActivityInstanceApi;
use OpenAPI\Client\ApiException;

class HistoricActivityInstanceService
{
    private HistoricActivityInstanceApi $api;

    public function __construct(HistoricActivityInstanceApi $api)
    {
        $this->api = $api;   
    }

    /**
     * Get List
     * 
     * @see https://github.com/fxkopp/camunda-php-sdk/blob/main/docs/Api/HistoricActivityInstanceApi.md#getHistoricActivityInstances
     * 
     * @param array $args 
     * @return Collection 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getHistoricActivityInstances($args = []): \Illuminate\Support\Collection
    {
        return collect(
            $this->api->getHistoricActivityInstances(
                data_get($args, 'sort_by'),
                data_get($args, 'sort_order'),
                data_get($args, 'first_result'),
                data_get($args, 'max_results'),
                data_get($args, 'activity_instance_id'),
                data_get($args, 'process_instance_id'),
                data_get($args, 'process_definition_id'),
                data_get($args, 'execution_id'),
                data_get($args, 'activity_id'),
                data_get($args, 'activity_name'),
                data_get($args, 'activity_type'),
                data_get($args, 'task_assignee'),
                data_get($args, 'finished'),
                data_get($args, 'unfinished'),
                data_get($args, 'canceled'),
                data_get($args, 'complete_scope'),
                data_get($args, 'started_before'),
                data_get($args, 'started_after'),
                data_get($args, 'finished_before'),
                data_get($args, 'finished_after'),
                data_get($args, 'tenant_id_in'),
                data_get($args, 'without_tenant_id')
            )
        )->map(function ($item) {
            return ActivityInstance::createFromHistoricActivityInstanceDto($item);
        });
    }
}