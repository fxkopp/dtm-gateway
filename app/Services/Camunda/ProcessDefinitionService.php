<?php

namespace App\Services\Camunda;

use App\Models\ProcessDefinition;
use App\Models\ProcessInstance;
use App\Models\VariableValue;
use Illuminate\Support\Collection;

use InvalidArgumentException;

use OpenAPI\Client\Api\ProcessDefinitionApi;
use OpenAPI\Client\ApiException;
use OpenAPI\Client\Model\StartProcessInstanceDto;

class ProcessDefinitionService
{
    private ProcessDefinitionApi $api;

    public function __construct(ProcessDefinitionApi $api)
    {
        $this->api = $api;
    }

    /**
     * Get
     * 
     * @see https://github.com/fxkopp/camunda-php-sdk/blob/main/docs/Api/ProcessDefinitionApi.md#getprocessdefinition
     * 
     * @param string $id The id of the process definition to be retrieved. (required)
     * @return ProcessDefinition 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getProcessDefinition(string $id): ProcessDefinition
    {
        return ProcessDefinition::createFromProcessDefinitionDto($this->api->getProcessDefinition($id));
    }

    /**
     * Get List
     * 
     * @see https://github.com/fxkopp/camunda-php-sdk/blob/main/docs/Api/ProcessDefinitionApi.md#getProcessDefinitions
     * 
     * @param array $args 
     * @return Collection 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getProcessDefinitions($args = []): \Illuminate\Support\Collection
    {
        return collect($this->api->getProcessDefinitions(
            data_get($args, 'process_definition_id'),
            data_get($args, 'process_definition_id_in'),
            data_get($args, 'name'),
            data_get($args, 'name_like'),
            data_get($args, 'deployment_id'),
            data_get($args, 'deployed_after'),
            data_get($args, 'deployed_at'),
            data_get($args, 'key'),
            data_get($args, 'keys_in'),
            data_get($args, 'key_like'),
            data_get($args, 'category'),
            data_get($args, 'category_like'),
            data_get($args, 'version'),
            data_get($args, 'latest_version'),
            data_get($args, 'resource_name'),
            data_get($args, 'resource_name_like'),
            data_get($args, 'startable_by'),
            data_get($args, 'active'),
            data_get($args, 'suspended'),
            data_get($args, 'incident_id'),
            data_get($args, 'incident_type'),
            data_get($args, 'incident_message'),
            data_get($args, 'incident_message_like'),
            data_get($args, 'tenant_id_in'),
            data_get($args, 'without_tenant_id'),
            data_get($args, 'include_process_definitions_without_tenant_id'),
            data_get($args, 'version_tag'),
            data_get($args, 'version_tag_like'),
            data_get($args, 'without_version_tag'),
            data_get($args, 'startable_in_tasklist'),
            data_get($args, 'not_startable_in_tasklist'),
            data_get($args, 'startable_permission_check'),
            data_get($args, 'sort_by'),
            data_get($args, 'sort_order'),
            data_get($args, 'first_result'),
            data_get($args, 'max_results'),
        ))->map(function ($item) {
            return ProcessDefinition::createFromProcessDefinitionDto($item);
        });
    }

    /**
     * Get Start Form Variables
     * 
     * @param string $key 
     * @return Collection 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function getStartFormVariablesByKey(string $key) : \Illuminate\Support\Collection
    {
        return 
            collect($this->api->getStartFormVariablesByKey($key))
            ->map(function ($item) {
                return VariableValue::createFromVariableValueDto($item);
            });
    }

    /**
     * Start Instance
     * 
     * Starts the latest version of a ProcessDefinition and returns the started ProcessInstance.
     * 
     * @param string $key Key of the ProcessDefinition.
     * @param array $data Optional array of data.
     * @return ProcessInstance 
     * @throws ApiException 
     * @throws InvalidArgumentException 
     */
    public function startProcessInstanceByKey(string $key, $data = []): ProcessInstance
    {
        $processInstanceDto = $this->api->startProcessInstanceByKey($key, new StartProcessInstanceDto($data));
        
        $processInstance = new ProcessInstance();

        return tap($processInstance->fill([
            'business_key' => $processInstanceDto->getBusinessKey(),
            'process_instance_id' => $processInstanceDto->getId(),
            'process_definition_key' => $key,
            'process_definition_id' => $processInstanceDto->getDefinitionId(),
        ]))->save();
    }
}