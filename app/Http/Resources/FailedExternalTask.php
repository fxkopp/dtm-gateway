<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FailedExternalTask extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'business_key' => $this->business_key,
            'process_instance_id' => $this->process_instance_id,
            'activity_id' => $this->activity_id,
            'worker_id' =>  $this->worker_id,
            'topic_name' => $this->topic_name,
            'error_message' => $this->error_message,
        ];
    }
}
