<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Incident extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'incident_id' => $this->id,
            'activity_id' => $this->activity_id,
            'incident_type' => $this->incident_type,
            'incident_message' => $this->incident_message,
            'failed_external_task_id' =>  $this->configuration,
            'incident_timestamp' => $this->incident_timestamp->format('Y-m-d H:i:s'),
        ];
    }
}
