<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ActivityInstance as ActivityInstanceResource;

class ProcessInstance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'process_instance_id' => $this->process_instance_id,
            'business_key' => $this->business_key,
            'process_definition_key' => $this->process_definition_key,
            'process_definition_id' => $this->process_definition_id,
            'name' => $this->name,
            'description' => $this->description,
            'state' => $this->state,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activity_instances' => ActivityInstanceResource::collection($this->activity_instances),
        ];
    }
}
