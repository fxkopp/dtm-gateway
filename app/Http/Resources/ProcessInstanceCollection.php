<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProcessInstanceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) {
            return [
                'process_instance_id' => $item->process_instance_id,
                'business_key' => $item->business_key,
                'process_definition_key' => $item->process_definition_key,
                'process_definition_id' => $item->process_definition_id,
                'name' => $item->name,
                'description' => $item->description,
                'state' => $item->state,
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at,
            ];
        });
    }
}
