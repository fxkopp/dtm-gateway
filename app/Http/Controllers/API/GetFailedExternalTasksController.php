<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\FailedExternalTask as FailedExternalTaskResource;
use App\Models\ProcessInstance;
use App\Services\Camunda\ExternalTaskService;

class GetFailedExternalTasksController
{
    protected ExternalTaskService $camundaService;

    public function __construct(ExternalTaskService $camundaService)
    {
        $this->camundaService = $camundaService;
    }

    public function __invoke(ProcessInstance $processInstance)
    {
        return FailedExternalTaskResource::collection(
            $this->camundaService->getFailedExternalTasks([
                'process_instance_id_in' => $processInstance->process_instance_id,
            ])
        );
    }
}
