<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProcessInstance as ProcessInstanceResource;
use App\Http\Resources\ProcessInstanceCollection;
use App\Models\ProcessInstance;
use Illuminate\Http\Request;

class ProcessInstanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ProcessInstanceCollection(ProcessInstance::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProcessInstance  $processInstance
     * @return \Illuminate\Http\Response
     */
    public function show(ProcessInstance $processInstance)
    {
        return new ProcessInstanceResource($processInstance);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProcessInstance  $processInstance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProcessInstance $processInstance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProcessInstance  $processInstance
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProcessInstance $processInstance)
    {
        //
    }
}
