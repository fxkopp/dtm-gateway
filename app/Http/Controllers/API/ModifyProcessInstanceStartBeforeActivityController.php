<?php

namespace App\Http\Controllers\API;

use App\Models\ProcessInstance;
use App\Actions\Camunda\ModifyProcessInstanceStartAction;
use App\Http\Requests\ModifyProcessInstanceStartRequest;

class ModifyProcessInstanceStartBeforeActivityController
{
    public function __invoke(ModifyProcessInstanceStartRequest $request, ProcessInstance $processInstance, ModifyProcessInstanceStartAction $modifyProcessInstanceStartAction)
    {
        $modifyProcessInstanceStartAction->execute(
            $processInstance,
            'startBeforeActivity',
            $request->input('activity_id')
        );
    }
}