<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\FailedExternalTask as FailedExternalTaskResource;
use App\Models\ProcessInstance;
use App\Services\Camunda\ExternalTaskService;

class ResolveFailedExternalTasksController
{
    protected ExternalTaskService $camundaService;

    public function __construct(ExternalTaskService $camundaService)
    {
        $this->camundaService = $camundaService;
    }

    public function __invoke(ProcessInstance $processInstance, string $externalTaskId)
    {
        $this->camundaService->setExternalTaskResourceRetries($externalTaskId, 1);
    }
}
