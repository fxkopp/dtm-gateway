<?php

namespace App\Http\Controllers\API;

use App\Actions\Camunda\UpdateProcessInstanceSuspensionStateAction;
use App\Models\ProcessInstance;
use App\Http\Resources\ProcessInstance as ProcessInstanceResource;

class ResumeProcessInstanceController
{
    public function __invoke(ProcessInstance $processInstance, UpdateProcessInstanceSuspensionStateAction $updateProcessInstanceSuspensionStateAction)
    {
        return new ProcessInstanceResource($updateProcessInstanceSuspensionStateAction->execute(
            $processInstance,
            false, // suspended?
        ));
    }
}
