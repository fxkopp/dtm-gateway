<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\VariableValue as VariableValueResource;
use App\Services\Camunda\ProcessDefinitionService;

class GetStartFormVariablesController
{
    protected ProcessDefinitionService $camundaService;

    public function __construct(ProcessDefinitionService $camundaService)
    {
        $this->camundaService = $camundaService;
    }

    public function __invoke(string $processDefinitionKey)
    {
        return VariableValueResource::collection(
            $this->camundaService->getStartFormVariablesByKey($processDefinitionKey)
        );
    }
}
