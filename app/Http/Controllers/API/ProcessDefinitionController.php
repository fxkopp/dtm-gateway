<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProcessDefinition as ProcessDefinitionResource;
use App\Services\Camunda\ProcessDefinitionService;
use Illuminate\Http\Request;

class ProcessDefinitionController
{
    protected ProcessDefinitionService $camundaService;

    public function __construct(ProcessDefinitionService $camundaService)
    {
        $this->camundaService = $camundaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProcessDefinitionResource::collection(
            $this->camundaService->getProcessDefinitions([
                'latest_version' => true,
            ])
        );
    }
}
