<?php

namespace App\Http\Controllers\API;

use App\Actions\Camunda\TerminateProcessInstanceAction;
use App\Models\ProcessInstance;

class TerminateProcessInstanceController
{
    public function __invoke(ProcessInstance $processInstance, TerminateProcessInstanceAction $terminateProcessInstanceAction)
    {
        $terminateProcessInstanceAction->execute($processInstance);
    }
}