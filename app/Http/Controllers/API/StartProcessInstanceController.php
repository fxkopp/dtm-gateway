<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProcessInstance as ProcessInstanceResource;
use App\Actions\Camunda\StartProcessInstanceAction;
use App\Http\Requests\StartProcessInstanceRequest;

class StartProcessInstanceController
{
    public function __invoke(StartProcessInstanceRequest $request, StartProcessInstanceAction $startProcessInstanceAction, string $processDefinitionKey)
    {
        return new ProcessInstanceResource($startProcessInstanceAction->execute(
            $processDefinitionKey,
            $request->input('name'),
            $request->input('business_key'),
            $request->input('description')
        ));
    }
}
