<?php

namespace App\Actions\Camunda;

use App\Jobs\SyncProcessInstance;
use App\Models\ProcessInstance;
use App\Models\ActivityInstance;
use App\Services\Camunda\ProcessInstanceService;

class TerminateProcessInstanceAction
{
    private ProcessInstanceService $processInstanceService;

    public function __construct(ProcessInstanceService $processInstanceService)
    {
        $this->processInstanceService = $processInstanceService;
    }

    public function execute(ProcessInstance $processInstance): void
    {
        // Log user action
        $processInstance->activity_instances()->save(ActivityInstance::createFromUserAction(
            'User_ProcessInstanceTerminate', // Activity ID
            'Terminate process instance' // Activity name
        ));

        // Get running ActivityInstances for Process Instance
        $this->processInstanceService->getActivityInstances($processInstance->process_instance_id)
            ->each(function ($item) use ($processInstance) {
                // Add cancel instruction for each
                $processInstance->addCancelActivityInstanceInstruction($item->activity_instance_id);
            });

        $this->processInstanceService->modifyProcessInstance($processInstance, 'Process instance terminated by user.');

        // Set active user tasks manually to 'ended' in log
        ActivityInstance::where('process_instance_id', $processInstance->process_instance_id)
            ->where('activity_type', 'userTask')
            ->whereNull('end_time')
            ->update(['end_time' => now()]);

        // Update process instance
        $processInstance->state = 'EXTERNALLY_TERMINATED';
        $processInstance->save();

        // Sync Process Instance state
        SyncProcessInstance::dispatch($processInstance);
    }
}
