<?php

namespace App\Actions\Camunda;

use App\Jobs\SyncProcessInstance;
use App\Models\ActivityInstance;
use App\Models\ProcessInstance;
use App\Services\Camunda\ProcessInstanceService;

class ModifyProcessInstanceStartAction
{
    private ProcessInstanceService $processInstanceService;

    public function __construct(ProcessInstanceService $processInstanceService)
    {
        $this->processInstanceService = $processInstanceService;
    }

    public function execute(ProcessInstance $processInstance, string $startType, string $activityId): void
    {
        // Get running ActivityInstances for Process Instance
        $this->processInstanceService->getActivityInstances($processInstance->process_instance_id)
            ->each(function($item) use($processInstance) {
                // Add cancel instruction for each
                $processInstance->addCancelActivityInstanceInstruction($item->activity_instance_id);
            });

        // Depending on startType, add start instruction
        if ($startType == 'startBeforeActivity') {
            $processInstance->addStartBeforeActivityInstruction($activityId);
        } else {
            $processInstance->addStartAfterActivityInstruction($activityId);
        }

        // Set active user tasks manually to 'ended' in log
        ActivityInstance::where('process_instance_id', $processInstance->process_instance_id)
            ->where('activity_type', 'userTask')
            ->whereNull('end_time')
            ->update(['end_time' => now()]);

        $this->processInstanceService->modifyProcessInstance($processInstance, 'Process instance terminated by user.');

        // Sync Process Instance state
        SyncProcessInstance::dispatch($processInstance);
    }
}