<?php

namespace App\Actions\Camunda;

use App\Models\ActivityInstance;
use App\Services\Camunda\ProcessDefinitionService;
use App\Models\ProcessInstance;

class StartProcessInstanceAction
{
    private ProcessDefinitionService $processDefinitionService;

    public function __construct(ProcessDefinitionService $processDefinitionService)
    {
        $this->processDefinitionService = $processDefinitionService;
    }

    public function execute(string $processDefinitionKey, string $name, ?string $businessKey = null, ?string $description = null): ProcessInstance
    {
        // Generate random business key unless one is given
        $businessKey = $businessKey ?? ProcessInstance::generateBusinessKey();

        // Use CamundaService to call API and store new ProcessInstance
        $processInstance = $this->processDefinitionService->startProcessInstanceByKey($processDefinitionKey, [
            'business_key' => $businessKey,
        ]);

        // Store additional data in DB
        $processInstance->fill([
            'name' => $name,
            'description' => $description ?? null,
            'state' => 'ACTIVE',
        ])->save();

        // Log user action
        $processInstance->activity_instances()->save(ActivityInstance::createFromUserAction(
            'User_ProcessInstanceStart', // Activity ID
            'Start process instance', // Activity name
        ));

        return $processInstance;
    }

}