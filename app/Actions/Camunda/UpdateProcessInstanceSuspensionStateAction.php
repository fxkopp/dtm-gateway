<?php

namespace App\Actions\Camunda;

use App\Jobs\SyncProcessInstance;
use App\Models\ActivityInstance;
use App\Models\ProcessInstance;
use App\Services\Camunda\ProcessInstanceService;

class UpdateProcessInstanceSuspensionStateAction
{
    private ProcessInstanceService $processInstanceService;

    public function __construct(ProcessInstanceService $processInstanceService)
    {
        $this->processInstanceService = $processInstanceService;
    }

    public function execute(ProcessInstance $processInstance, bool $suspended): ProcessInstance
    {
        // Log user action
        $processInstance->activity_instances()->save(ActivityInstance::createFromUserAction(
            ($suspended == true) ? 'User_ProcessInstanceSuspend' : 'User_ProcessInstanceResume', // Activity ID
            ($suspended == true) ? 'Suspend process instance' : 'Resume process instance' // Activity name
        ));

        // Update suspension state
        $this->processInstanceService->updateSuspensionState(
            $processInstance->process_instance_id,
            $suspended
        );

        // Update process instance
        $processInstance->state = ($suspended == true) ? 'SUSPENDED' : 'ACTIVE';
        $processInstance->save();

        // Sync Process Instance state
        SyncProcessInstance::dispatch($processInstance);

        return $processInstance;
    }
}