<?php

namespace App\Jobs;

use App\Models\ActivityInstance;
use App\Models\ProcessInstance;
use App\Services\Camunda\HistoricActivityInstanceService;
use App\Services\Camunda\HistoricProcessInstanceService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncProcessInstance implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private ProcessInstance $processInstance;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ProcessInstance $processInstance)
    {
        $this->processInstance = $processInstance;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HistoricProcessInstanceService $historicProcessInstanceService, HistoricActivityInstanceService $historicActivityInstanceService)
    {
        /**
         * Sync Process Instance status
         */
        if ($this->processInstance->isActive() || is_null($this->processInstance->state)) {
            // Call API to retrieve Historic Process Instance
            $historicProcessInstance = $historicProcessInstanceService->getHistoricProcessInstance($this->processInstance->process_instance_id);
            // Update Process Instance status
            $this->processInstance->state = $historicProcessInstance->state;
            if ($this->processInstance->isDirty()) $this->processInstance->save();
        }

        /**
         * Sync Activity Instances that belong to the Process Instance
         */
        
        // Call API to retrieve Historic Activity Instances
        $historicActivityInstances = $historicActivityInstanceService->getHistoricActivityInstances([
            'process_instance_id' => $this->processInstance->process_instance_id,
        ]);

        // Create or - if present in DB - Update each Activity Instance
        foreach ($historicActivityInstances as $historicActivityInstance) {

            $activityInstance = ActivityInstance::where('activity_instance_id', $historicActivityInstance->activity_instance_id)->first();

            // Check if record is already present
            if (! is_null($activityInstance)) {
                // Only Update if instance did not already end
                if (is_null($activityInstance->end_time)) {
                    $activityInstance->update($historicActivityInstance->attributesToArray());
                }
            } else {
                // Create new entry
                ActivityInstance::create($historicActivityInstance->attributesToArray());
            }
        }
    }
}
