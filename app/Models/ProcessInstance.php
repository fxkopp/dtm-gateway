<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use OpenAPI\Client\Model\HistoricProcessInstanceDto;

class ProcessInstance extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The primary key associated with the table.
     * 
     * @see https://laravel.com/docs/8.x/eloquent#primary-keys
     *
     * @var string
     */
    protected $primaryKey = 'process_instance_id';

    /**
     * Indicates if the model's ID is auto-incrementing.
     * 
     * @see https://laravel.com/docs/8.x/eloquent#primary-keys
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The data type of the auto-incrementing ID.
     * 
     * @see https://laravel.com/docs/8.x/eloquent#primary-keys
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Collection of modification instructions
     * 
     * @var Collection
     */
    public Collection $modificationInstructions;

    /**
     * Get the activity instances for the process instance.
     */
    public function activity_instances()
    {
        return $this->hasMany(ActivityInstance::class, 'process_instance_id', 'process_instance_id')->orderByRaw('ISNULL(end_time) DESC, end_time DESC, start_time DESC');
    }

    public function __construct()
    {
        $this->modificationInstructions = collect([]);
    }

    /**
     * The "booted" method of the model.
     * 
     * Generate primary key at object creation.
     * 
     * @see https://laravel.com/docs/8.x/eloquent#applying-global-scopes
     *
     * @return void
     */
    public static function booted(): void
    {
        static::creating(function (ProcessInstance $processInstance) {
            if (is_null($processInstance->business_key)) 
                $processInstance->business_key = ProcessInstance::generateBusinessKey();
        });
    }

    /**
     * Return new instance of self with provided data.
     * 
     * @param HistoricProcessInstanceDto $historicProcessInstanceDto 
     * @return ProcessInstance 
     */
    public static function createFromHistoricProcessInstanceDto(\OpenAPI\Client\Model\HistoricProcessInstanceDto $historicProcessInstanceDto): self
    {
        $instance = new self();

        $instance->process_instance_id = (string) $historicProcessInstanceDto->getId();
        $instance->business_key = (string) $historicProcessInstanceDto->getBusinessKey();
        $instance->process_definition_key = (string) $historicProcessInstanceDto->getProcessDefinitionKey();
        $instance->state = $historicProcessInstanceDto->getState();

        return $instance;
    }

    /**
     * Generate a random business key for a process instance.
     * 
     * @return string 
     */
    public static function generateBusinessKey(): string
    {
        return (string) Str::uuid();
    }

    /**
     * Returns true if process instance is active
     * 
     * @return bool 
     */
    public function isActive(): bool
    {
        return $this->state === 'ACTIVE';
    }

    /**
     * Returns true if process instance is suspended
     * 
     * @return bool 
     */
    public function isSuspended(): bool
    {
        return $this->state === 'SUSPENDED';
    }

    /**
     * Returns true if process instance is completed
     * 
     * @return bool 
     */
    public function isCompleted(): bool
    {
        return $this->state === 'COMPLETED';
    }
    
    /**
     * Returns true if process instance is terminated
     * 
     * @return bool 
     */
    public function isTerminated(): bool
    {
        return ($this->state === 'EXTERNALLY_TERMINATED' || $this->state === 'INTERNALLY_TERMINATED');
    }

    /**
     * Requests cancellation of all instances of one activity
     * 
     * @param string $activityId 
     * @return ProcessInstanceService 
     */
    public function addCancelActivityInstruction(string $activityId): self
    {
        $this->modificationInstructions->push([
                'type' => 'cancel',
                'activity_id' => $activityId,
            ]
        );

        return $this;
    }

       /**
     * Requests cancellation of a single activity instance
     * 
     * @param string $activityInstanceId 
     * @return ProcessInstanceService 
     */
    public function addCancelActivityInstanceInstruction(string $activityInstanceId): self
    {
        $this->modificationInstructions->push([
                'type' => 'cancel',
                'activity_instance_id' => $activityInstanceId,
            ]
        );

        return $this;
    }

    /**
     * Requests to enter a given activity
     * 
     * @param string $activityId 
     * @return ProcessInstanceService 
     */
    public function addStartBeforeActivityInstruction(string $activityId): self
    {
        $this->modificationInstructions->push([
                'type' => 'startBeforeActivity',
                'activity_id' => $activityId,
            ]
        );

        return $this;
    }

    /**
     * Requests to execute the single outgoing sequence flow of a given activity
     * 
     * @param string $activityId 
     * @return ProcessInstanceService 
     */
    public function addStartAfterActivityInstruction(string $activityId): self
    {
        $this->modificationInstructions->push([
                'type' => 'startAfterActivity',
                'activity_id' => $activityId,
            ]
        );

        return $this;
    }

    /**
     * Reset instructions that have not been sent already
     * 
     * @return ProcessInstanceService 
     */
    public function clearInstructions(): self
    {
        $this->modificationInstructions = collect([]);

        return $this;
    }


}
