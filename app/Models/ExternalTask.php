<?php

namespace App\Models;

class ExternalTask
{
    public string $id;

    public string $business_key;

    public string $process_instance_id;

    public string $activity_id;

    public string $worker_id;

    public string $topic_name;

    public string $error_message;

    public static function createFromExternalTaskDto(\OpenAPI\Client\Model\ExternalTaskDto $externalTaskDto): self
    {
        $instance = new self();

        $instance->id = (string) $externalTaskDto->getId();
        $instance->business_key = (string) $externalTaskDto->getBusinessKey();
        $instance->process_instance_id = (string) $externalTaskDto->getProcessInstanceId();
        $instance->activity_id = (string) $externalTaskDto->getActivityId();
        $instance->worker_id = (string) $externalTaskDto->getWorkerId();
        $instance->topic_name = (string) $externalTaskDto->getTopicName();
        $instance->error_message = (string) $externalTaskDto->getErrorMessage();

        return $instance;
    }

}
