<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OpenAPI\Client\Model\ActivityInstanceDto;
use OpenAPI\Client\Model\HistoricActivityInstanceDto;

class ActivityInstance extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @see https://laravel.com/docs/8.x/eloquent#timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * All of the relationships to be touched.
     * 
     * @see https://laravel.com/docs/8.x/eloquent-relationships#touching-parent-timestamps
     *
     * @var array
     */
    protected $touches = ['process_instance'];

    /**
     * The attributes that should be cast.
     * 
     * @see https://laravel.com/docs/8.x/eloquent-mutators#array-and-json-casting
     *
     * @var array
     */
    protected $casts = [
        'activity_canceled' => 'boolean',
        'activity_data' => 'array',
        'start_time' => 'datetime',
        'end_time' => 'datetime'
    ];

    /**
     * Get the process instance that owns the activity instance.
     */
    public function process_instance()
    {
        return $this->belongsTo(ProcessInstance::class, 'process_instance_id', 'process_instance_id');
    }

    /**
     * Create ActivityInstance from HistoricActivityInstance API
     * 
     * @param HistoricActivityInstanceDto $historicActivityInstanceDto 
     * @return ActivityInstance 
     */
    public static function createFromHistoricActivityInstanceDto(\OpenAPI\Client\Model\HistoricActivityInstanceDto $historicActivityInstanceDto): self
    {
        $instance = new self();

        $instance->activity_instance_id = (string) $historicActivityInstanceDto->getId();
        $instance->activity_id = (string) $historicActivityInstanceDto->getActivityId();
        $instance->activity_name = (string) $historicActivityInstanceDto->getActivityName();
        $instance->activity_type = (string) $historicActivityInstanceDto->getActivityType();

        $instance->activity_canceled = (boolean) $historicActivityInstanceDto->getCanceled();

        $instance->activity_source = 'api';

        $instance->process_instance_id = (string) $historicActivityInstanceDto->getProcessInstanceId();
        $instance->called_process_instance_id = (string) $historicActivityInstanceDto->getCalledCaseInstanceId();

        $instance->start_time = $historicActivityInstanceDto->getStartTime();
        $instance->end_time = $historicActivityInstanceDto->getEndTime();

        return $instance;
    }

    /**
     * Create ActivityInstance from ProcessInstance API
     * 
     * @param ActivityInstanceDto $activityInstanceDto 
     * @return ActivityInstance 
     */
    public static function createFromActivityInstanceDto(\OpenAPI\Client\Model\ActivityInstanceDto $activityInstanceDto): self
    {
        $instance = new self();

        $instance->activity_instance_id = (string) $activityInstanceDto->getId();
        $instance->activity_id = (string) $activityInstanceDto->getActivityId();
        $instance->activity_name = (string) $activityInstanceDto->getActivityName();
        $instance->activity_type = (string) $activityInstanceDto->getActivityType();

        $instance->activity_source = 'api';

        $instance->process_instance_id = (string) $activityInstanceDto->getProcessInstanceId();
        
        $instance->start_time = now();

        return $instance;
    }

    /**
     * Create ActivityInstance from UserAction
     * 
     * @param string $activityId 
     * @param string $activityName 
     * @param array $activityData 
     * @param string $activityType 
     * @param bool $autoEnd 
     * @return ActivityInstance 
     */
    public static function createFromUserAction(string $activityId, string $activityName, array $activityData = [], string $activityType = 'userAction', bool $autoEnd = true): self
    {
        $instance = new self();

        $instance->activity_id = $activityId;
        $instance->activity_name = $activityName;
        $instance->activity_type = $activityType;

        $instance->activity_source = 'user';

        $instance->activity_data = $activityData;

        $instance->start_time = now()->subSecond();
        if ($autoEnd) $instance->end_time = $instance->start_time;

        return $instance;
    }
}
