<?php

namespace App\Models;

class Incident
{
    public string $id;

    public string $incident_type;

    public string $activity_id;

    public string $incident_message;

    public string $configuration;

    public $incident_timestamp;

    public static function createFromIncidentDto(\OpenAPI\Client\Model\IncidentDto $incidentDto): self
    {
        $instance = new self();

        $instance->id = $incidentDto->getId();
        $instance->incident_timestamp = $incidentDto->getIncidentTimestamp();
        $instance->incident_type = (string) $incidentDto->getIncidentType();
        $instance->activity_id = (string) $incidentDto->getActivityId();
        $instance->incident_message = (string) $incidentDto->getIncidentMessage();
        $instance->configuration = (string) $incidentDto->getConfiguration();

        return $instance;
    }
}
