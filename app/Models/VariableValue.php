<?php

namespace App\Models;

use Illuminate\Contracts\Support\Arrayable;

class VariableValue implements Arrayable
{

    public mixed $value;

    public string $type;

    public static function createFromVariableValueDto(\OpenAPI\Client\Model\VariableValueDto $variableValueDto): self
    {
        $instance = new self();

        $instance->value = is_array($variableValueDto->getValue()) ? implode(" ", (array) $variableValueDto->getValue()) : $variableValueDto->getValue();
        $instance->type = (string) $variableValueDto->getType();

        return $instance;
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'type' => $this->type,
            'value' => $this->value,
        ];
    }
}