<?php

namespace App\Models;

class ProcessDefinition
{
    public string $id;

    public string $key;

    public string $description;

    public string $name;

    public int $version;

    public bool $startable_in_tasklist;

    public static function createFromProcessDefinitionDto(\OpenAPI\Client\Model\ProcessDefinitionDto $processDefinitionDto): self
    {
        $instance = new self();

        $instance->id = (string) $processDefinitionDto->getId();
        $instance->key = (string) $processDefinitionDto->getKey();
        $instance->description = (string) $processDefinitionDto->getDescription();
        $instance->name = (string) $processDefinitionDto->getName();
        $instance->version = (int) $processDefinitionDto->getVersion();
        $instance->startable_in_tasklist = (bool) $processDefinitionDto->getStartableInTasklist();

        return $instance;
    }
}
