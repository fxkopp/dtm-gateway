<?php

use App\Http\Controllers\API\ResumeProcessInstanceController;
use App\Http\Controllers\API\GetStartFormVariablesController;
use App\Http\Controllers\API\ProcessDefinitionController;
use App\Http\Controllers\API\ProcessInstanceController;
use App\Http\Controllers\API\StartProcessInstanceController;
use App\Http\Controllers\API\SuspendProcessInstanceController;
use App\Http\Controllers\API\TerminateProcessInstanceController;
use App\Http\Controllers\API\ModifyProcessInstanceStartBeforeActivityController;
use App\Http\Controllers\API\ModifyProcessInstanceStartAfterActivityController;
use App\Http\Controllers\API\GetFailedExternalTasksController;
use App\Http\Controllers\API\ResolveFailedExternalTasksController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/process-definitions', [ProcessDefinitionController::class, 'index']);

Route::get('/process-definitions/{processDefinitionKey}/form-variables', GetStartFormVariablesController::class);

Route::post('/process-definitions/{processDefinitionKey}/start', StartProcessInstanceController::class);

Route::get('/process-instances', [ProcessInstanceController::class, 'index']);

Route::get('/process-instances/{processInstance}', [ProcessInstanceController::class, 'show']);

Route::post('/process-instances/{processInstance}/suspend', SuspendProcessInstanceController::class);

Route::post('/process-instances/{processInstance}/resume', ResumeProcessInstanceController::class);

Route::post('/process-instances/{processInstance}/terminate', TerminateProcessInstanceController::class);

Route::post('/process-instances/{processInstance}/startBeforeActivity', ModifyProcessInstanceStartBeforeActivityController::class);

Route::post('/process-instances/{processInstance}/startAfterActivity', ModifyProcessInstanceStartAfterActivityController::class);

Route::get('/process-instances/{processInstance}/failedExternalTasks', GetFailedExternalTasksController::class);

Route::post('/process-instances/{processInstance}/failedExternalTasks/{externalTaskId}/resolve', ResolveFailedExternalTasksController::class);

Route::get('/test', [ProcessDefinitionController::class, 'test']);

// Route::apiResource('/process-definitions', ProcessDefinitionController::class);

